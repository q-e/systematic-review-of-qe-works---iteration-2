# Systematic Review of Works in Quantitative Ethnography

This is the continuation of our pilot study on the systematic review of the proceedings from the first International Conference for Quantitative Ethnography, which can be reached here: https://gitlab.com/q-e/icqe-2019-sysrev.

## Objectives and Means

### Main objective of project

Hold a “disciplinary mirror” up to the QE community – this is a general motivation to provide information to the community about methodological, thematic, conceptual, and reporting choices QE researchers have made so far in their publications. The idea of a “mirror” is descriptive, rather than prescriptive; i.e. we are looking to describe what we see in the publications, and our “sight” is limited to the items we are coding for.
 
### Vehicle of achieving objective

Conduct a systematic review – through a rigorous process of selecting and coding publications, and then aggregating the data in various forms, such as (most fundamentally) on our rendered Gitlab page and in further communications.
 
### Relevance of objective

Open communication about methodology would benefit the development of QE as a discipline. Firstly, as with any theoretical framework, the QE framework is informed and shaped by its specific operationalizations in various projects. Secondly, scientific rigor and the advancement of best practices is spurred by openly accessible data, procedures, and well-documented decisions. Thirdly, as QE evolves and expands, novel methods and tools will be developed, which are more effectively disseminated through open communication. Our present study, and our broader ongoing systematic review, is an Open Science initiative to facilitate methodological discourse in the QE community.

## Authors

Savannah Donegan (University of Wisconsin-Madison), Brendan Eagan (University of Wisconsin-Madison), Anna Geröly (ELTE, Hungary), Marcia Moraes (Colorado State University, USA), Gjalt-Jorn Peters (Open University of the Netherlands), Clare Porter (University of Wisconsin-Madison), Szilvia Zörgő (Semmelweis University, Hungary)

## URLs

The main analysis file of this project is an R Markdown file, the rendered version of which is hosted by GitLab pages at https://q-e.gitlab.io/systematic-review-of-qe-works. This main repository of this project is hosted at the Open Science Framework at https://osf.io/pa9jv/ and at GitLab at https://gitlab.com/q-e/systematic-review-of-qe-works.
