Our rxs specification sheet can be accessed at this URL:
https://docs.google.com/spreadsheets/d/1n6bZaWyHAHTw6pzoS9Z5IaZ_i0xiW3XHfrir2NfPSO8/edit#gid=0

This Excel is composed of several tabs:

ValueTemplates - Discloses the type of values that are valid and/or set to default in the extraction template
Entities - Lists the entities (codes) that are being extracted from publications (parents and children), their identifier, and their valid values.
Definitions - Contains the descriptions for each entity (code) and specific instructions for identifying each one
Tips - Discloses some suggestions for going about the coding process (distinct from coding instructions)